#language: pt

@apiTestes
Funcionalidade: Validar API de carrinho de compras

Cenario: Validar estrutura e tipos de dados retornados da API
Dado a URI de carrinho de compras da API
Quando faco uma solicitacao de busca usando GET
Entao o sistema retorna o status 200
E a validacao do schema esta correta

Cenario: Validar se o numero do carrinho de compras esta de acordo com a quantidade de itens
Dado a URI de carrinho de compras do mock
Quando faco uma solicitacao de busca usando GET
Entao o sistema retorna o status 200
E a validacao dos itens esta correta

Esquema do Cenario: Adicionar produto ao carrinho com sucesso
Dado a URI de carrinho de compras do mock
E passando um body com as informacoes do produto a ser adicionado: "<sku>","<color>","<size>","<price>"
Quando faco uma solicitacao de criacao usando POST
Entao o sistema retorna o status 201
E o sistema retorna mensagem de produto adicionado com sucesso
Exemplos: 
|sku	   | color  | size | price |
| demo_3 | Orange | S    | 26.00 |

Cenario: Remover produto do carrinho com sucesso
Dado a URI de carrinho de compras do mock
Quando faco uma solicitacao de remocao usando DELETE passando o SKU do produto igual a: "demo_3"
Entao o sistema retorna o status 200
E o sistema retorna mensagem de produto removido com sucesso

Cenario: Tentar remover produto inexistente do carrinho
Dado a URI de carrinho de compras do mock
Quando faco uma solicitacao de remocao usando DELETE passando o SKU do produto igual a: "demo_9"
Entao o sistema retorna o status 200
E o sistema retorna mensagem de produto inexistente no carrinho
