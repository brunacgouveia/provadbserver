#language: pt

@uiTestes
Funcionalidade: Realizar compra sem cadastro previo
Como um usuario,
Eu gostaria de adicionar produtos no carrinho e fazer cadastro
Para poder comprar produtos sem precisar realizar cadastro previo

Esquema do Cenario: Realizar uma compra com sucesso
Dado um usuario que se encontra na pagina principal da loja
E nao esteja logado no sistema
Quando adiciona um produto no carrinho 
E confere que a quantidade de produtos adicionados e "1" 
E prossegue para o passo de autenticacao
E cria um usuario informando somente campos obrigatorios: "<email>","<nome>","<sobrenome>","<password>","<endereco>","<cidade>","<estado>","<cep>","<celular>" e "<tipoEndereco>"
E confere se o endereco esta correto: "<endereco>"
E prossegue para o passo de entrega
E aceita os termos do servico de entrega 
E prossegue para o passo de pagamento
E confere o valor total da compra
E seleciona o metodo de pagamento via banco
E confirma a compra
Entao a compra e finalizada com sucesso

Exemplos:
	| email					 | nome 	| sobrenome | senha		 | endereco | cidade | estado | cep   | celular   | tipoEndereco |
	| user@gmail.com | Fulano | da Silva 	| teste123 | rua 123  | Cidade | Alaska | 99999 | 999988888 | Residencial  |
		