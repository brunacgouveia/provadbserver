package steps.api;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.equalTo;

import org.json.JSONObject;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

public class GerenciarCarrinhoComprasSteps {
	private Response response;
	private ValidatableResponse json;
	private RequestSpecification request;
	private String url = "";
	
	@Dado("a URI de carrinho de compras da API")
	public void a_URI_de_carrinho_de_compras_da_API() {
	    System.out.println("Dado a URI de carrinho de compras da API");
	    url = "http://5d9cc58566d00400145c9ed4.mockapi.io/shopping_cart";
		request = given().contentType("application/json");
	}
	
	@Dado("a URI de carrinho de compras do mock")
	public void a_URI_de_carrinho_de_compras_do_mock() {
	    System.out.println("Dado a URI de carrinho de compras do mock");
	    url = "http://localhost:9876/shopping_cart";
		request = given().contentType("application/json");
	}
	
	@Dado("passando um body com as informacoes do produto a ser adicionado: {string},{string},{string},{string}")
	public void passando_um_body_com_as_informacoes_do_produto_a_ser_adicionado(String sku, String color, String size, String price) {
	    System.out.println("Dado passando um body com as informa��es do produto a ser adicionado: SKU: "+sku+" / color: "+color+" / size: "+size+" / price: "+price);
	    
	    JSONObject jsonObj = new JSONObject();
	    jsonObj.append("sku", sku).append("color", color).append("size", size).append("price", price);

	    request.body(jsonObj.toString());
	    System.out.println("Body: "+jsonObj.toString());
	}

	@Quando("faco uma solicitacao de busca usando GET")
	public void faco_uma_solicitacao_de_busca_usando_GET() {
		System.out.println("Quando fa�o uma solicita��o de busca usando GET");
		
		response = request.when().log().all().get(url);
		
		System.out.println("RESPONSE BODY: "+response.asString());
		System.out.println("RESPONSE CONTENT TYPE: "+response.getContentType());
		System.out.println("RESPONSE STATUS LINE: "+response.getStatusLine());
		System.out.println("RESPONSE TIME: "+response.getTime()+" ms");	
	}
	
	@Quando("faco uma solicitacao de criacao usando POST")
	public void faco_uma_solicitacao_de_criacao_usando_POST() {
	    System.out.println("Quando fa�o uma solicita��o de cria��o usando POST");

	    response = request.when().log().all().post(url);
		
		System.out.println("RESPONSE BODY: "+response.asString());
		System.out.println("RESPONSE CONTENT TYPE: "+response.getContentType());
		System.out.println("RESPONSE STATUS LINE: "+response.getStatusLine());
		System.out.println("RESPONSE TIME: "+response.getTime()+" ms");	
	}

	@Quando("faco uma solicitacao de remocao usando DELETE passando o SKU do produto igual a: {string}")
	public void faco_uma_solicitacao_de_remocao_usando_DELETE_passando_o_produto(String sku) {
	    System.out.println("Quando fa�o uma solicita��o de remo��o usando DELETE passando o SKU do produto igual a: "+ sku);
	    
	    response = request.when().log().all().delete(url+"/"+sku);
		
		System.out.println("RESPONSE BODY: "+response.asString());
		System.out.println("RESPONSE CONTENT TYPE: "+response.getContentType());
		System.out.println("RESPONSE STATUS LINE: "+response.getStatusLine());
		System.out.println("RESPONSE TIME: "+response.getTime()+" ms");	
	}
	
	@Entao("o sistema retorna o status {int}")
	public void o_sistema_retorna_o_status(Integer statusCode) {
		System.out.println("Ent�o o sistema retorna o status "+statusCode);
		
		json = response.then().statusCode(statusCode);
	}

	@Entao("a validacao do schema esta correta")
	public void a_validacao_do_schema_esta_correta() {
		System.out.println("Ent�o a valida��o do schema est� correta");
		
		json = json.body(matchesJsonSchemaInClasspath("schema.json"));
	}	

	@Entao("a validacao dos itens esta correta")
	public void a_validacao_dos_items_esta_correta() {
	    System.out.println("Ent�o a valida��o dos itens est� correta");
	    
	    JSONObject jsonObj = new JSONObject(response.asString());   
		int shopping_cart = jsonObj.getInt("shopping_cart");
		
		json = json.body("sku", hasSize(shopping_cart))
				.body("price", hasSize(shopping_cart))
				.body("size", hasSize(shopping_cart))
				.body("color", hasSize(shopping_cart));
	}

	@Entao("o sistema retorna mensagem de produto adicionado com sucesso")
	public void o_sistema_retorna_mensagem_de_produto_adicionado_com_sucesso() {
	    System.out.println("Ent�o o sistema retorna mensagem de produto adicionado com sucesso");
	    
	    json = json.body("message", equalTo("Produto adicionado ao carrinho com sucesso"));
	}

	@Entao("o sistema retorna mensagem de produto removido com sucesso")
	public void o_sistema_retorna_mensagem_de_produto_removido_com_sucesso() {
	    System.out.println("Ent�o o sistema retorna mensagem de produto removido com sucesso");
	    
	    json = json.body("message", equalTo("Produto removido do carrinho com sucesso"));
	}

	@Entao("o sistema retorna mensagem de produto inexistente no carrinho")
	public void o_sistema_retorna_mensagem_de_produto_nao_existente() {
	    System.out.println("Ent�o o sistema retorna mensagem de produto inexistente no carrinho");
	    
	    json = json.body("message", equalTo("Produto nao existe no carrinho"));
	}
}
