package steps.ui;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;

import org.apache.tools.ant.util.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import driver.DriverFactory;
import io.cucumber.java.After;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;
import ui.facade.ShoppingCartFacade;
import ui.facade.UserAuthenticationFacade;
import ui.pageobject.HomePagePO;

public class RealizarCompraSteps {
	
	private ShoppingCartFacade shoppingCart = new ShoppingCartFacade(DriverFactory.getDriver());
	private UserAuthenticationFacade userAuthentication = new UserAuthenticationFacade(DriverFactory.getDriver()); 
	private HomePagePO homePage = new HomePagePO(DriverFactory.getDriver());
	
	@Dado("um usuario que se encontra na pagina principal da loja")
	public void um_usuario_que_se_encontra_na_pagina_principal_da_loja() {
		System.out.println("Dado um usu�rio que se encontra na p�gina principal da loja");
		if(! homePage.isHomePageDisplayed())
			DriverFactory.acessURL(HomePagePO.getHomeURL());
	}
	
	@Dado("nao esteja logado no sistema")
	public void nao_esteja_logado_no_sistema() {
		System.out.println("Dado nao esteja logado no sistema");
		if(homePage.isUserLoggedIn())
			homePage.logUserOut();	
	}

	@Quando("adiciona um produto no carrinho")
	public void adiciona_um_produto_no_carrinho() {
		System.out.println("Quando adiciona um produto no carrinho");
		shoppingCart.addAnyProductToCart();
	}

	@Quando("confere que a quantidade de produtos adicionados e {string}")
	public void confere_que_a_quantidade_de_produtos_adicionados_e(String quantidade) {
		System.out.println("Quando confere que a quantidade de produtos adicionados � "+quantidade);
		takeScreenshotFullPage("validarProdutoCarrinho");
		Assert.assertTrue("Produto n�o adicionado corretamente", shoppingCart.validateQuantityOfProducts(quantidade));
	}

	@Quando("prossegue para o passo de autenticacao")
	public void prossegue_para_o_passo_de_autenticacao() {
		System.out.println("Quando prossegue para o passo de autentica��o");
		shoppingCart.goToSignInStep();
	}
	
	@Quando("cria um usuario informando somente campos obrigatorios: {string},{string},{string},{string},{string},{string},{string},{string},{string} e {string}")
	public void cria_um_usuario_informando_somente_campos_obrigatorios(String email, String nome, String sobrenome, String senha, String endereco, String cidade, String estado, String cep, String celular, String tipoEndereco) {
		System.out.println("Quando cria um usu�rio informando somente campos obrigat�rios");
		String date = new SimpleDateFormat("yyMMddHHmm-").format(new Date()); //adicionando identificador unico ao email
		userAuthentication.createNewAccountWithMandatoryFields(date+email, nome, sobrenome, senha, endereco, cidade, estado, cep, celular, tipoEndereco);		
	}
	
	@Quando("confere se o endereco esta correto: {string}")
	public void confere_se_o_endereco_esta_correto(String endereco) {
		System.out.println("Quando confere se o endereco esta correto");
		takeScreenshotFullPage("validarEndereco");
		Assert.assertTrue("Endere�o n�o confere", shoppingCart.validateAddress(endereco));	
	}

	@Quando("prossegue para o passo de entrega")
	public void prossegue_para_o_passo_de_entrega() {
		System.out.println("Quando prossegue para o passo de entrega");
		shoppingCart.goToShippingStep();
	}
	
	@Quando("aceita os termos do servico de entrega")
	public void aceita_os_termos_do_servico_de_entrega() {
		System.out.println("Quando aceita os termos do servico de entrega");
		shoppingCart.acceptShippingTermOfService();
	}
	
	@Quando("prossegue para o passo de pagamento")
	public void prossegue_para_o_passo_de_pagamento() {
		System.out.println("Quando prossegue para o passo de pagamento");
		shoppingCart.goToPaymentStep();
	}

	@Quando("confere o valor total da compra")
	public void confere_o_valor_total_da_compra() {
		System.out.println("Quando confere o valor total da compra");
		takeScreenshotFullPage("validarValorTotal");
		Assert.assertTrue("Valor total n�o confere", shoppingCart.validateTotalPriceAtPaymentStep());
	}

	@Quando("seleciona o metodo de pagamento via banco")
	public void seleciona_o_metodo_de_pagamento_via_banco() {
		System.out.println("Quando seleciona o m�todo de pagamento via banco");
		shoppingCart.payByBankWire();
	}

	@Quando("confirma a compra")
	public void confirma_a_compra() {
		System.out.println("Quando confirma a compra");
		shoppingCart.confirmOrderPayment();
	}

	@Entao("a compra e finalizada com sucesso")
	public void a_compra_e_finalizada_com_sucesso() {
		System.out.println("Ent�o a compra � finalizada com sucesso");
		takeScreenshotFullPage("validarConfirmacaoCompra");
		Assert.assertTrue("Compra n�o confirmada", shoppingCart.validateConfirmationPage());	
	}
	
	@After("@uiTestes")
	public void tearDown() {
		takeScreenshotViewableArea();
	}
	
	public void takeScreenshotViewableArea(){
		System.out.println("Salvando screenshot...");	
		
		String date = new SimpleDateFormat("yy-MM-dd_HH-mm-ss").format(new Date());		
		try { 
			TakesScreenshot screenshot = (TakesScreenshot)DriverFactory.getDriver();
			File srcFile = screenshot.getScreenshotAs(OutputType.FILE);
			File tgtFile = new File(System.getProperty("user.dir")+"\\src\\test\\resources\\images\\img_" + date + ".png");

			FileUtils.getFileUtils().copyFile(srcFile, tgtFile); 
		} catch(Exception e) { 
			System.out.println(e);
		}		 				
	}
	
	public void takeScreenshotFullPage(String fileName) {
		String date = new SimpleDateFormat("yy-MM-dd_HH-mm-ss").format(new Date());	
		Screenshot screenshot=new AShot().shootingStrategy(ShootingStrategies.viewportPasting(ShootingStrategies.scaling(1.5f), 1000)).takeScreenshot(DriverFactory.getDriver());             
		try {                 
			 ImageIO.write(screenshot.getImage(),"PNG",new File(System.getProperty("user.dir")+"\\src\\test\\resources\\images\\"+fileName+"_" + date + ".png"));             
		} 
		catch (IOException e) {  
			System.out.println(e);
			e.printStackTrace();             
		}                                             
	} 

}
