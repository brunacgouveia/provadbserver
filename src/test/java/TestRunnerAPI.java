import java.io.UnsupportedEncodingException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import com.aventstack.extentreports.ExtentReports;
import com.github.tomakehurst.wiremock.WireMockServer;

import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.Cucumber;	

@RunWith(Cucumber.class)				
@CucumberOptions(features= {"src/test/resources/features/api"},
				 plugin={"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"})	
	
public class TestRunnerAPI {
		
	public static ExtentReports extentReports;
	public static WireMockServer wireMockServer;
	
	@BeforeClass
	public static void start() {		
		System.out.println("[LOG] Iniciando os testes...");
		
		wireMockServer = new WireMockServer(9876);
		wireMockServer.start();
		
		extentReports = new ExtentReports();
		try {
			extentReports.setGherkinDialect("pt");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
	@AfterClass
	public static void tearDown() {
		System.out.println("[LOG] Finalizando os testes...");
		wireMockServer.stop();
		extentReports.flush();
	}
	
}
