package ui.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProductAddedToCartPopupPO extends BasePO{
	
	@FindBy(xpath="//div[@id='layer_cart']//a[@title='Proceed to checkout']")
	private WebElement btnProceedToCheckout;
	
	public ProductAddedToCartPopupPO(WebDriver driver) {
		super(driver);
	}
	
	public void proceedToCheckout() {
		btnProceedToCheckout.click();
	}
	
	public boolean isProductAddedToCartPopupDisplayed() {
		return isElementVisible(btnProceedToCheckout);
	}
}
