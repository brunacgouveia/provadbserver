package ui.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ShoppingCartShippingPO extends BasePO {
	
	@FindBy(id="uniform-cgv")
	private WebElement cckTermOfService;
	
	@FindBy(name="processCarrier")
	private WebElement btnProceedToCheckout;
	
	public ShoppingCartShippingPO(WebDriver driver) {
		super(driver);
	}
	
	public void acceptTermOfService() {
		cckTermOfService.click();
	}
	
	public void clickProceedToCheckoutButton() {
		btnProceedToCheckout.click();
	}
	
	public boolean isShoppinCartShippingPageDisplayed() {		
		return isElementVisible(cckTermOfService);
	}
}
