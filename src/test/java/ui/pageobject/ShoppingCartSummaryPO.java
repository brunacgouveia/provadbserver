package ui.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ShoppingCartSummaryPO extends BasePO{
	@FindBy(xpath="//div[@id='order-detail-content']/..//a[@title='Proceed to checkout']")
	private WebElement btnProceedToCheckout;
	
	@FindBy(id="order-detail-content")
	private WebElement divProductDetail;
	
	@FindBy(id="summary_products_quantity")
	private WebElement lblQuantityOfProducts;
	
	public ShoppingCartSummaryPO(WebDriver driver) {
		super(driver);
	}
	
	public void proceedToCheckout() {
		btnProceedToCheckout.click();
	}
	
	public boolean isShoppingCartSummaryPageDisplayed() {
		return isElementVisible(btnProceedToCheckout);
	}
	
	public boolean isProductInformationDisplayed() {
		return isElementVisible(divProductDetail);
	}
	
	public String getQuantityOfProducts() {
		return lblQuantityOfProducts.getText().replaceAll("\\D+","");
	}
}
