package ui.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class HomePagePO extends BasePO{
		
	@FindBy(id="search_query_top")
	private WebElement txtSearch;
	
	@FindBy(name="submit_search")
	private WebElement btnSearch;
	
	@FindBy(xpath="//a[@class='product_img_link']")
	private WebElement lnkfirstProduct;
	
	@FindBy(xpath="//li[contains(@class,'hovered')]//a[@title='Add to cart']")
	private WebElement btnAddToCart;
	
	@FindBy(xpath="//a[@title='Log me out']")
	private WebElement lnkSignOut;
	
		
	public HomePagePO(WebDriver driver) {
		super(driver);
	}
			
	public void typeSearchProduct(String productName) {
		txtSearch.clear();
		txtSearch.sendKeys(productName);
	}
	
	public void clickSearchButton() {
		btnSearch.click();
	}
	
	public void putMouseOverFirstProduct() {
		putMouseOver(lnkfirstProduct);
	}
		
	public void clickAddProductToCartButton() {
		btnAddToCart.click();
	}
	
	public boolean isHomePageDisplayed() {		
		return isElementVisible(lnkfirstProduct);
	}
	
	public static String getHomeURL() {
		return "http://automationpractice.com/";
	}
	
	public boolean isUserLoggedIn() {
		return isElementVisible(lnkSignOut);
	}
	
	public void logUserOut() {
		lnkSignOut.click();
	}
	
	
}
