package ui.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AuthenticationPO extends BasePO{
	
	@FindBy(id="email_create")
	private WebElement txtEmailAddressCreateAccount;

	@FindBy(id="SubmitCreate")
	private WebElement btnCreateAnAccount;
	
	public AuthenticationPO(WebDriver driver) {
		super(driver);
	}
	
	public void typeEmailToCreateAnAccount(String email) {
		txtEmailAddressCreateAccount.sendKeys(email);
	}
	
	public void clickCreateAnAccountButton() {
		btnCreateAnAccount.click();
	}
	
	public boolean isShoppingCartAuthenticationPageDisplayed() {
		return isElementVisible(btnCreateAnAccount);
	}

}
