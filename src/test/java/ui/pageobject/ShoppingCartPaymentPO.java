package ui.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ShoppingCartPaymentPO extends BasePO {
	
	@FindBy(id = "total_product")
	private WebElement lblTotalProduct;
	
	@FindBy(id = "total_shipping")
	private WebElement lblTotalShipping;
	
	@FindBy(id = "total_price")
	private WebElement lblTotalPrice;
	
	@FindBy(how = How.CLASS_NAME, using = "bankwire")
	private WebElement btnPayByBankWire;
	
	@FindBy(how = How.CLASS_NAME, using = "cheque")
	private WebElement btnPayByCheck;
	
	@FindBy(xpath="//span[text()='I confirm my order']/..")
	private WebElement btnConfirmMyOrder;
	
	@FindBy(id="order-confirmation")
	private WebElement lblOrderConfirmation;

	public ShoppingCartPaymentPO(WebDriver driver) {
		super(driver);
	}
	
	public float getTotalProductValue() {
		return Float.parseFloat(lblTotalProduct.getText().replace("$", ""));
	}
	
	public float getTotalShippingValue() {
		return Float.parseFloat(lblTotalShipping.getText().replace("$", ""));
	}
	
	public float getTotalPriceValue() {
		return Float.parseFloat(lblTotalPrice.getText().replace("$", ""));
	}
	
	public void clickPayByBankWireButton() {
		btnPayByBankWire.click();
	}
	
	public void clickPayByCheckButton() {
		btnPayByCheck.click();
	}
	
	public void clickIConfirmMyOrderButton() {
		btnConfirmMyOrder.click();
	}
	
	public boolean isShoppingCartPaymentPageDisplayed() {
		return isElementVisible(btnPayByBankWire);
	}
	
	public boolean isConfirmationPageDisplayed() {
		return isElementVisible(lblOrderConfirmation);
	}
}
