package ui.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class CreateAnAccountPO extends BasePO{
	
	@FindBy(id="customer_firstname")
	private WebElement txtFirstNamePersonalInformation;
	
	@FindBy(id="customer_lastname")
	private WebElement txtLastNamePersonalInformation;
	
	@FindBy(id="passwd")
	private WebElement txtPassword;
	
	@FindBy(id="address1")
	private WebElement txtAddress1;
	
	@FindBy(id="city")
	private WebElement txtCity;

	@FindBy(id="id_state")
	private WebElement cmbState;
	
	@FindBy(id="postcode")
	private WebElement txtZipPostalCode;
		
	@FindBy(id="phone_mobile")
	private WebElement txtMobilePhone;
	
	@FindBy(id="alias")
	private WebElement txtAddressAlias;
	
	@FindBy(id="submitAccount")
	private WebElement btnRegister;
	
	
	public CreateAnAccountPO(WebDriver driver) {
		super(driver);
	}
	
	public void typeFirstName(String firstName) {
		txtFirstNamePersonalInformation.sendKeys(firstName);
	}
	
	public void typeLastName(String lastName) {
		txtLastNamePersonalInformation.sendKeys(lastName);
	}
	
	public void typePassword(String password) {
		txtPassword.sendKeys(password);
	}
	
	public void typeAddressLine1(String addressLine1) {
		txtAddress1.sendKeys(addressLine1);
	}
	
	public void typeCity(String city) {
		txtCity.sendKeys(city);
	}
	
	public void selectState(String state) {
		Select dropdown = new Select(cmbState);
		dropdown.selectByVisibleText(state);
	}
	
	public void typeZipPostalCode(String zipPostalCode) {
		txtZipPostalCode.sendKeys(zipPostalCode);
	}
	
	public void typeMobilePhone(String mobilePhone) {
		txtMobilePhone.sendKeys(mobilePhone);
	}
	
	public void typeAddressAlias(String addressAlias) {
		txtAddressAlias.clear();
		txtAddressAlias.sendKeys(addressAlias);
	}
	
	public void clickRegisterButton() {
		btnRegister.click();
	}
	
	public boolean isCreateAnAccountPageDisplayed() {
		return isElementVisible(txtFirstNamePersonalInformation);
	}

}
