package ui.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ShoppingCartAddressPO extends BasePO{

	@FindBy(xpath="//ul[@id='address_delivery']/li[contains(@class,'address_firstname')]")
	private WebElement lblFirstLastNameDelivery;
	
	@FindBy(xpath="//ul[@id='address_delivery']/li[contains(@class,'address_address1')]")
	private WebElement lblAddress1and2Delivery;
	
	@FindBy(xpath="//ul[@id='address_delivery']/li[contains(@class,'address_city')]")
	private WebElement lblCityStatePostcodeDelivery;
	
	@FindBy(xpath="//ul[@id='address_delivery']/li[contains(@class,'address_country_name')]")
	private WebElement lblCountryDelivery;
	
	@FindBy(xpath="//ul[@id='address_delivery']/li[contains(@class,'address_phone_mobile')]")
	private WebElement lblMobilePhoneDelivery;
	
	@FindBy(name="processAddress")
	private WebElement btnProceedToCheckout;
	
	
	public ShoppingCartAddressPO(WebDriver driver) {
		super(driver);
	}
	
	public String getFirstNameLastNameDelivery() {
		return lblFirstLastNameDelivery.getText();
	}
	
	public String getAddress1And2Delivery() {
		return lblAddress1and2Delivery.getText();		
	}
	
	public String getCityStatePostCodeDelivery() {
		return lblCityStatePostcodeDelivery.getText();
	}
	
	public String getCountryDelivery() {
		return lblCountryDelivery.getText();
	}
	
	public String getMobilePhoneDelivery() {
		return lblMobilePhoneDelivery.getText();
	}
	
	public void clickProceedToCheckoutButton() {
		btnProceedToCheckout.click();
	}
	
	public boolean isShoppinCartAddressPageDisplayed() {		
		return isElementVisible(lblFirstLastNameDelivery);
	}
}
