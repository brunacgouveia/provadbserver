package ui.pageobject;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePO {
	
    protected final WebDriver driver;
    protected final WebDriverWait wait;	
    protected final Actions action;
    private static final long TIMEOUT = 3;
    
    protected BasePO(WebDriver driver){
        this.driver = driver;
        wait = new WebDriverWait(driver, 1);
        action = new Actions(driver);
        initElements();
    }
    
    private void initElements() {
        PageFactory.initElements(driver, this);
    }
    
    protected void putMouseOver(WebElement element) {
		waitUntilVisible(element);    	
    	action.moveToElement(element).build().perform();
	}
    
    protected boolean isElementVisible(WebElement element){
		try {
			waitUntilVisible(element);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
    
    private void waitUntilVisible(WebElement element) {
    	wait.withTimeout(Duration.ofSeconds(TIMEOUT));
        wait.until(ExpectedConditions.visibilityOfAllElements(element));
    }   
}
