package ui.facade;

import org.openqa.selenium.WebDriver;

import ui.pageobject.HomePagePO;
import ui.pageobject.ProductAddedToCartPopupPO;
import ui.pageobject.ShoppingCartAddressPO;
import ui.pageobject.ShoppingCartPaymentPO;
import ui.pageobject.ShoppingCartShippingPO;
import ui.pageobject.ShoppingCartSummaryPO;

public class ShoppingCartFacade {
	
	private HomePagePO homePage;
	private ProductAddedToCartPopupPO productAddedToCartPopUp;
	private ShoppingCartSummaryPO summary;
	private ShoppingCartAddressPO address; 
	private ShoppingCartShippingPO shipping;
	private ShoppingCartPaymentPO payment;
	
	public ShoppingCartFacade(WebDriver driver) {
		homePage = new HomePagePO(driver);
		productAddedToCartPopUp = new ProductAddedToCartPopupPO(driver);
		summary = new ShoppingCartSummaryPO(driver);
		address = new ShoppingCartAddressPO(driver); 
		shipping = new ShoppingCartShippingPO(driver);
		payment = new ShoppingCartPaymentPO(driver);
	}
	
	public void addAnyProductToCart() {
		homePage.isHomePageDisplayed();
		homePage.putMouseOverFirstProduct();
		homePage.clickAddProductToCartButton();

		productAddedToCartPopUp.proceedToCheckout();
	}
	
	public void searchAndAddProductToCart(String product) {
		homePage.typeSearchProduct(product);
		homePage.clickSearchButton();
		homePage.putMouseOverFirstProduct();
		homePage.clickAddProductToCartButton();

		productAddedToCartPopUp.proceedToCheckout();
	}
	
	public boolean validateQuantityOfProducts(String quantity) {		
		if((summary.getQuantityOfProducts().equals(quantity)) && (summary.isProductInformationDisplayed()))
			return true;
		else
			return false;
	}
	
	public void goToSignInStep() {
		summary.proceedToCheckout();
	}
	
	public boolean validateAddress(String addressLine1) {
		if(address.getAddress1And2Delivery().equals(addressLine1))
			return true;
		else
			return false;		
	}
	
	public void goToShippingStep() {
		address.clickProceedToCheckoutButton();
	}
	
	public void acceptShippingTermOfService() {
		shipping.acceptTermOfService();
	}
	
	public void goToPaymentStep() {
		shipping.clickProceedToCheckoutButton();
	}
	
	public boolean validateTotalPriceAtPaymentStep() {
		float totalPrice = payment.getTotalProductValue() + payment.getTotalShippingValue();
		
		if(totalPrice == payment.getTotalPriceValue())
			return true;
		else
			return false;		
	}
	
	public void payByBankWire() {						
		payment.clickPayByBankWireButton();			
	}
	
	public void confirmOrderPayment() {				
		payment.clickIConfirmMyOrderButton();				
	}
	
	public boolean validateConfirmationPage() {
		return payment.isConfirmationPageDisplayed();
	}

}
