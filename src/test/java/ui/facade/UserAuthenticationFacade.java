package ui.facade;

import org.openqa.selenium.WebDriver;

import ui.pageobject.AuthenticationPO;
import ui.pageobject.CreateAnAccountPO;

public class UserAuthenticationFacade {

	private	AuthenticationPO authentication;
	private CreateAnAccountPO createAnAccount; 
	
	public UserAuthenticationFacade(WebDriver driver) {
 		authentication = new AuthenticationPO(driver);
		createAnAccount = new CreateAnAccountPO(driver); 
	}
	
	public void createNewAccountWithMandatoryFields(String email, String firstName, String lastName, String password, String addressLine1, 
			String city, String state, String zipPostalCode, String mobilePhone, String addressAlias) {
		
		authentication.typeEmailToCreateAnAccount(email);
		authentication.clickCreateAnAccountButton();

		createAnAccount.typeFirstName(firstName);
		createAnAccount.typeLastName(lastName);
		createAnAccount.typePassword(password);
		createAnAccount.typeAddressLine1(addressLine1);
		createAnAccount.typeCity(city);
		createAnAccount.selectState(state);
		createAnAccount.typeZipPostalCode(zipPostalCode);
		createAnAccount.typeMobilePhone(mobilePhone);
		createAnAccount.typeAddressAlias(addressAlias);
		createAnAccount.clickRegisterButton();		
	}
}
