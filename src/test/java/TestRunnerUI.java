import java.io.UnsupportedEncodingException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import com.aventstack.extentreports.ExtentReports;

import driver.DriverFactory;
import io.cucumber.junit.CucumberOptions;
import ui.pageobject.HomePagePO;
import io.cucumber.junit.Cucumber;	

@RunWith(Cucumber.class)				
@CucumberOptions(features= {"src/test/resources/features/ui"},
				 plugin={"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"})	
	
public class TestRunnerUI {
		
	public static ExtentReports extentReports;
	
	@BeforeClass
	public static void start() {		
		System.out.println("[LOG] Iniciando os testes...");
		
		extentReports = new ExtentReports();
		try {
			extentReports.setGherkinDialect("pt");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		DriverFactory.startDriver();		
		DriverFactory.acessURL(HomePagePO.getHomeURL());
	}
	
	@AfterClass
	public static void tearDown() {
		System.out.println("[LOG] Finalizando os testes...");
		DriverFactory.closeDriver();
		extentReports.flush();
	}
	
}
