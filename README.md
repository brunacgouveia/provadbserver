# testeAPI_brunaGouveia

Este projeto tem o objetivo de testar automaticamente alguns cen�rios de aplica��es UI e API.</p>
**UI**: <br>
- *URL base*: http://automationpractice.com <br>
- *Linguagens utilizadas*: Selenium WebDriver, Java e Cucumber <br>
- *Cen�rios*: <br>
	- Realizar uma compra com sucesso
	
**API**: <br>
- *URL base*: http://5d9cc58566d00400145c9ed4.mockapi.io/shopping_cart <br>
- *Linguagens utilizadas*: Rest Assured, Java e Cucumber. <br>
- *Cen�rios*: <br>
	- Validar estrutura e tipos de dados retornados da API<br>
	- Validar se o n�mero do carrinho de compras est� de acordo com a quantidade de itens<br>
	- Adicionar produto ao carrinho com sucesso<br>
	- Remover produto do carrinho com sucesso<br>
	- Tentar remover produto inexistente do carrinho<p>


## Pr�-requisitos

As especifica��es de m�quina e ferramentas utilizadas para criar e executar a aplica��o foram:
- Windows 10 64 bits
- Eclipse IDE 2018-12, com o plugin para Cucumber instalado
- JRE 8u201
- Chrome v83.0.4103.116


## Como executar

**Projeto UI**
1. Fazer um clone do reposit�rio para a m�quina local.
2. Abrir o projeto como maven no Eclipse ou outra IDE de prefer�ncia.
3. Abrir o arquivo de teste que est� na pasta "/src/test/java/TestRunnerUI.java".
4. Executar o arquivo utilizando JUnit 4.<br>
	- As imagens geradas durante a execu��o s�o salvas na pasta "src/test/resources/images/".<br>
	- O relat�rio de execu��o gerado � salvo na pasta "src/test/resources/reports/".

**Projeto API**
1. Fazer um clone do reposit�rio para a m�quina local.
2. Importar o projeto como maven para o Eclipse ou outra IDE de prefer�ncia.
3. Abrir o arquivo de teste que est� na pasta "/src/test/java/TestRunnerAPI.java".
4. Executar o arquivo utilizando JUnit 4.<br>
	- O relat�rio de execu��o gerado � salvo na pasta "src/test/resources/reports/".
